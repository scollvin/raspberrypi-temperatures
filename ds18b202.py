#!/usr/bin/python3

from w1thermsensor import W1ThermSensor
import time
import json
import argparse
import warnings
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import requests
import pydim
import signal
import sys
import os
from pathlib import Path




def create_daemon(pid_file, user_name):
    """Detach a process from the controlling terminal and run it in the
    background as a daemon.
    http://code.activestate.com/recipes/278731/
    Arguments:
        - pid_file, complete path to the file where we'll write the pid
        - user_name, user name which will run the process
    """
    # Default daemon parameters.
    # File mode creation mask of the daemon.
    UMASK = 0

    # Default working directory for the daemon.
    WORKDIR = str('/') #root, so that it exists in every distribution

    # Default maximum for the number of available file descriptors.
    MAXFD = 1024

    #Set the user through the argument
    USERNAME = int(user_name)

    # The standard I/O file descriptors are redirected to /dev/null by default.
    if (hasattr(os, "devnull")):
        REDIRECT_TO = os.devnull
    else:
        REDIRECT_TO = "/dev/null"
    try:
        # Fork a child process so the parent can exit.  This returns control to
        # the command-line or shell.  It also guarantees that the child will not
        # be a process group leader, since the child receives a new process ID
        # and inherits the parent's process group ID.  This step is required
        # to insure that the next call to os.setsid is successful.
        pid = os.fork()
    except OSError as e:
        raise Exception("%s [%d]" % (e.strerror, e.errno))

    if pid == 0:	# The first child.
        # To become the session leader of this new session and the process group
        # leader of the new process group, we call os.setsid().  The process is
        # also guaranteed not to have a controlling terminal.
        os.setsid()

        try:
            # Fork a second child and exit immediately to prevent zombies.  This
            # causes the second child process to be orphaned, making the init
            # process responsible for its cleanup.  And, since the first child is
            # a session leader without a controlling terminal, it's possible for
            # it to acquire one by opening a terminal in the future (System V-
            # based systems).  This second fork guarantees that the child is no
            # longer a session leader, preventing the daemon from ever acquiring
            # a controlling terminal.
            pid = os.fork()	# Fork a second child.
        except OSError as e:
            raise Exception ("%s [%d]" % (e.strerror, e.errno))

        if pid == 0:	# The second child.
            # Since the current working directory may be a mounted filesystem, we
            # avoid the issue of not being able to unmount the filesystem at
            # shutdown time by changing it to the root directory.
            os.chdir(WORKDIR)
            # We probably don't want the file mode creation mask inherited from
            # the parent, so we give the child complete control over permissions.
            os.umask(UMASK)
            
        else:
            os._exit(0)	# Exit parent (the first child) of the second child.
    else:
        os._exit(0)	# Exit parent of the first child.
    # Use the getrlimit method to retrieve the maximum file descriptor number
    # that can be opened by this process.  If there is not limit on the
    # resource, use the default value.
    import resource		# Resource usage information.
    maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
    if maxfd == resource.RLIM_INFINITY:
        maxfd = MAXFD
    
    # Iterate through and close all file descriptors.
    for fd in range(maxfd):
        try:
            os.close(fd)
        except OSError:	# ERROR, fd wasn't open to begin with (ignored)
            pass

    # Redirect the standard I/O file descriptors to the specified file.  Since
    # the daemon has no controlling terminal, most daemons redirect stdin,
    # stdout, and stderr to /dev/null.  This is done to prevent side-effects
    # from reads and writes to the standard I/O file descriptors.

    # This call to open is guaranteed to return the lowest file descriptor,
    # which will be 0 (stdin), since it was closed above.
    os.open(REDIRECT_TO, os.O_RDWR)	# standard input (0)

    # Duplicate standard input to standard output and standard error.
    os.dup2(0, 1)			# standard output (1)
    os.dup2(0, 2)			# standard error (2)
    #write the pid to the file
    processID = str(os.getpid())
    open(pid_file, "w").write(processID + "\n")
    #Switch user
    os.setuid(USERNAME)

    return(0)
 
class NotEnoughInputsError (Exception):
    ''' There are some inputs missing that are not default.
    '''
    def __init__(self, msg, input_missing):
        '''Input missing:
        - 0: interval
        '''
        self.msg = msg
        self.input_missing = input_missing

class MismatchWarning (UserWarning):
  ''' Used to inform that either some devices or addresses of devices that were expected
  have not been found'''
  pass

def str2bool(v):
    '''Converts any string that can be understood as a boolean to a boolean'''
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
 
def config(config_file):
    '''Arguments:
    - config_file, name of json file with the configuration parameters 
      
    Returns a tuple with:
    ·if it takes a config file:
        - number_sensors, the number of sensors that are both detected and on the config file
        - list_sensors, a list of the sensor objects both detected and on the config file
        (in the same order as the file_addresses list)
        - file_addresses, a list with all the sensors addresses """"""
        - file_names, a list with the names of the sensors """""
        So it ignores any sensor that is not in the list and obviously, any sensor that is
        not found by the RBPi
        - dns, the name of the Domain Name System for the PyDIM
        - server_name, the name of the PyDIM server
        - service_address,  address and port to host telegraf HTTP listener on
        - data_file, name of the file to which the program will write the acquired data 
        - interval, [type int] interval (seconds) at which the temperatures are read and written 
        - file_write_file, list of boolean values to set if the sensor data will be writen into the file
        - file_write_telegraf, list of boolean values to set if the sensor data will be writen to telegraf
        - file_write_PyDIM, list of boolean values to set if the sensor data will be writen to PyDIM
    ·if it does not take the optional argument:
        - number_sensors_rbpi, the number of sensors detected by the RBPi
        - list_sensors_rbpi, a list of found sensor objects 
        - list_addresses_rbpi, a list with the sensors addresses found by the RBPi
     
    Raises:
    - a MismatchWarning warning if the addresses from the file do not match the ones found by the RBPi
    - a UserWarning if no config_file has been given

    *For changing the sensor's precision there's another script that has to be run under sudo.
    '''
    # W1ThermSensor.get_available_sensors() returns a list of sensor instances
    list_sensors_rbpi = W1ThermSensor.get_available_sensors() #sensor objects  
    number_sensors_rbpi = len(list_sensors_rbpi) 
    
    # make a list with the addresses found
    list_addresses_rbpi = []
    for sensor in list_sensors_rbpi:
        list_addresses_rbpi.append(sensor.id)

    if config_file:
        # Set empty lists where we'll store the config data for the sensors
        file_addresses = []
        file_names = []
        file_write_file = []
        file_write_telegraf = []
        file_write_PyDIM = []
        with open(config_file, 'r') as json_file:  
            conf_data = json.load(json_file) #is a dictionary
            #Get the different data form the config file
            PyDIM_conf = conf_data['PyDIM']
            telegraf_conf = conf_data['telegraf']
            file_conf = conf_data['data_file']
            time_conf = conf_data['time']
            sensors_conf = conf_data['sensors'] #list of dictionaries
            for i in range(len(sensors_conf)):
                address = sensors_conf[i]['address'] 
                name = sensors_conf[i]['name']
                number = sensors_conf[i]['number'] #maybe not needed...
                fil = str2bool(sensors_conf[i]['write to file'])
                tel = str2bool(sensors_conf[i]['write to telegraf'])
                py = str2bool(sensors_conf[i]['write to PyDIM'])
                file_addresses.append(address)
                file_names.append(name)
                #Create a list with True/False values for every sensor which will enable or disalbe
                #the writing options further on
                file_write_file.append(fil)
                file_write_telegraf.append(tel)
                file_write_PyDIM.append(py)
            dns = PyDIM_conf['dns']
            server_name = PyDIM_conf['server_name']
            service_address = telegraf_conf['service_address']
            data_file = file_conf['file_name']
            interval = int(time_conf['interval'])
            
        #Create a set (an unordered collection with no duplicate elements) with the adresses from the file
        afrom_file = set(file_addresses) 
 
         
        # Create a set with the addresses found on the raspberry
        afrom_rasp = set(list_addresses_rbpi)
        #Now we can compare the 2 sets:
        intersection = afrom_file & afrom_rasp
        if afrom_file != afrom_rasp :
            warnings.warn('The addresses on the file DO NOT match the ones found bye the RBPi', MismatchWarning)
            addresses_not_found = afrom_file - afrom_rasp #addresses in afrom_file but not in afrom_rasp
            addresses_missing_in_file = afrom_rasp - afrom_file #addresses in afrom_rasp but not in afrom_file
            if addresses_not_found: #If addresses_not_found is not an empty set
                print('The addresses not found on the RBPi are: ', addresses_not_found)
                #Remove the elements that have not been found by the RBPi from the corresponding lists
                for  address in addresses_not_found:
                    index = file_addresses.index(address)
                    del file_addresses[index]
                    del file_names[index]
                    del file_write_file[index]
                    del file_write_telegraf[index]
                    del file_write_PyDIM[index]
            if addresses_missing_in_file:
                print('The addresses missing on the file are: ', addresses_missing_in_file)
            # Sort list_sensors_rbpi so that the order of the sensor objects match the order of the file_addresses list
            # and save them in a new list, list_sensors:
            list_sensors = []
            for address in file_addresses:
                index = list_addresses_rbpi.index(address)
                list_sensors.append(list_sensors_rbpi[index])

            number_sensors = len(file_addresses) #also change the number of sensors
        else:
            print('The addresses on the file match the ones found bye the RBPi')
        return number_sensors, list_sensors, file_addresses, file_names, dns, \
               server_name, service_address, data_file, interval, file_write_file, \
               file_write_telegraf, file_write_PyDIM
    else:
        warnings.warn('No config file was given as a command line argument', UserWarning)
        return number_sensors_rbpi, list_sensors_rbpi, list_addresses_rbpi
    
def command_line_arguments():
    '''Defines the arguments that can be sent through the command line and returns their value.
    That is:
    - interval in seconds at which temperatures will be measured.
    - name of the configuration file.
    - name of the file to which the data will be written. 
    - Plot: set or unset the live plot. Default to false.

    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("--interval", "-i", type = int, \
        help="changes the values of the interval in s between reading temperatures")
    parser.add_argument("--config", "-c", type = str, #default = 'manual_config.json',  \
        help = "path of the configuration file" )
    parser.add_argument("--file", "-f", type = str, \
        help="name of the file to write the data")
    parser.add_argument("--plot", "-p", type = str, default = "False", \
        help="enables the plotting live option if set to True") 
    parser.add_argument("--daemon", "-d", type = str, default = "False", \
        help = "runs the program as a daemon if set to True")
    #Arguments that are only needed if daemon is True
    parser.add_argument("--pid_file", "-P", type = str, help = "the name of the pid file")
    parser.add_argument("--user_name", "-u", type=str, help = "the user which will run the program")

    args = parser.parse_args()
    interval = args.interval
    config_file = args.config
    data_file = args.file
    plotting = str2bool(args.plot)
    daemon = str2bool(args.daemon)
    pid_file = args.pid_file
    user_name = args.user_name

    return interval, config_file, data_file, plotting,daemon, pid_file, user_name

def read_temperatures(list_sensors, list_names):
    '''Reads the temperature for every sensor and returns a list of temperatures.
    If it finds an error, that is temperature = 85 or 127.5 C, it will print a warning and 
    try to read the temperature for 5 times. If it doesn't succeed it will return None. '''
    list_temp = []
    for sensor in list_sensors:
        temp = sensor.get_temperature() #read the temprature for each sensor 
        temp_correct = False
        counter = 0
        #Check if there's been an error in reading the temperature, and then read it again
        while temp_correct == False and counter < 5: 
            if temp == 85:
                index = list_sensors.index(sensor)
                warnings.warn(list_names[index] + ' sensor read temperature 85 C (the power-on reset value of the' \
                'temperature register). Counter: '+ str(counter), UserWarning) #print a warning to stardard error (check through journalctl if it runs as a service)
                temp = sensor.get_temperature()
            elif temp == 127.5:
                index = list_sensors.index(sensor)
                warnings.warn(list_names[index] + ' sensor read temperature 127.5 C. Counter: ' + str(counter), UserWarning)
                temp = sensor.get_temperature()
            else: 
                temp_correct = True #If all is correct, exit the while
            counter = counter + 1
        if temp_correct == False:
            warnings.warn('Couldn\'t read ' + list_names[index] + ' sensor temperature', UserWarning)
            list_temp.append(None) #append a None value to the temperatures list and continue with next iteration of the for
            continue
        list_temp.append(temp)
    return list_temp 

def write_header(number_sensors, data_file, list_names, write_file):
    '''Writes header of the data storing file.
    Arguments:
        - number_sensors, number of sensors enabled
        - data_file, name of the file to which the program will write the acquired data 
        - list_names, the list of names in the configuration file
        - write_file, list of boolean values which set if the sensor data is to be writen to the file
    Returns:
        - t_file, file object
    '''
    t_file = open(data_file,'a+') #append to the file
    t_file.write('#Date: ')
    t_file.write(time.asctime(time.localtime(time.time()))) #print human readable timestamp
    t_file.write('\n#\n')
    t_file.write('#execution time,')
    for i in range(number_sensors):
        if write_file[i] == True:
            t_file.write('   ' + list_names[i] + ',')  
    t_file.write('\n#\n')      
    return t_file

def write_to_file(number_sensors, t_file, list_temp, write_file): 
    '''Writes the temperature data collected from the sensors to a file.
    Arguments:
    - number_sensors, number of sensors enabled
    - t_file, the opened file object
    - list_temp, the list of temperatures collected by the read_temperatures function
    - write_file, list of boolean values which set if the sensor data is to be written to the file
    '''
    t_file.write(str(time.time()) + ',') #write timestamp (seconds from the epoch)
    for i in range(number_sensors):
        if write_file[i] == True:
            temp = list_temp[i]
            t_file.write("%.1f" % temp + ',') #for each sensor write the temperature with 1 decimal
    t_file.write('\n')
    t_file.flush() #flush the buffer so that the file is updated real-time
           
def write_to_telegraf(number_sensors, list_temp, list_names, write_telegraf, service_address):   
    '''Writes to telegraf the name of the sensor (under the label "sensor") and its temperature in the correct format.
    The subject / metrics is "temperatures".
    Arguments:
        - number_sensors, number of sensors enabled
        - list_temp, the list with the temperatures read from the sensors
        - list_names, list with the sensors' names
        - write_telegraf, list of boolean values which set if the sensor data is to be written to telegraf
        - service_address, address and port to host HTTP listener on (default argument)
    Prometheus IP address: http://10.128.223.38:9090. --> check if it's receiving data. (expression: temperatures{})
    - temperatures{sensor =~".*rack"}
    - temperatures{sensor =~"sensor [0-9]+"}
    '''
    session = requests.session() #Creates a new session
    session.trust_env = False #For the proxy
    telegraf_address = "http://" + service_address + "/write"

    for i in range(number_sensors):
        if write_telegraf[i] == True:
            temp = list_temp[i]
            #The syntax of InfluxDB Line Protocol sets that the space has to be escaped 
            # with the backslash character \
            name = list_names[i].replace(' ','\ ')
            telegraf_data = 'temperatures,sensor=' + str(name) + ' value=' + str(temp)
            session.post(telegraf_address, data = telegraf_data)
    return              

def init_pydim(number_sensors, list_names, write_PyDIM, dns, server_name):
    '''Initializes the services to write to PyDIM.
    Arguments:
        - number_sensors, number of sensors enabled
        - list_names, list of sensor names
        - write_PyDIM, list of boolean values which set if the sensor data is to be written to PyDIM
        - dns, the name of the Domain Name System for the PyDIM
        - server_name, a name for the server
    '''
    def callback(tag): #A callback function is needed although it does nothing ¯\_(ツ)_/¯ 
        pass
        return 
    list_services = []
    for i in range(number_sensors):
        if write_PyDIM[i] == True:
            # The function dis_add_service is used to register the service in DIM
            #It takes the name of the service, in this case one float value (that will be the temperature),
            #a callback function that does nothing, and a tag for the callback function
            svc = pydim.dis_add_service('service_' + str(list_names[i]), "F:1;", callback, i) 
            list_services.append(svc)
    # Start the DIM server
    pydim.dis_set_dns_node(dns) #The name of the DNS
    pydim.dis_start_serving(server_name) #The name under which the server is going to be registered in the DIM DNS

    return list_services

def write_to_pydim(number_sensors, list_services, list_temp, write_PyDIM):
    '''Updates the pydim services with the new temperature data.
    To check that everything is being published to PyDIM:
        - ssh -X user@lbgw
        - Ssh -Y plus
        and either: - export DIM_DNS_NODE=infmag01  #save the variable
                    - did
        or:         - DIM_DNS_NODE=infmag01 did
    '''
    j = 0 # will go over the elements of list_services. 
    for i in range(number_sensors):
        if write_PyDIM[i] == True:
            temp = list_temp[i]
            pydim.dis_update_service(list_services[j], (temp,))
            j = j+1
    return

def plot_live_and_file(number_sensors, list_sensors, list_names, interval):
    '''Reads the temperature from the sensors at real time, plots live, and in the current 
    directory saves to the file 'temperatures_plot.txt' and saves the image as "temp.png". 
    Maximum 15 sensors at a time.
    Parameters:
    - number_sensors, number of sensors enabled
    - list_sensors: list of sensor objects
    - list_names: list of names of the sensors if config file provided or an integer if not
    - interval, interval at which to read the temperature in seconds
    '''
    number_sensors = len(list_sensors) #number of sensors that we want to plot
    fig, ax = plt.subplots()
    #Create a list of line objects, each of which will store and display the data for every sensor
    lines = []
    for i in range(number_sensors):
        line = plt.Line2D([],[], linewidth = 2)
        lines.append(line)
        ax.add_line(line) #add the line to the figure that will be plotted

    ax.grid()    
    colors = "bgrcmykw" #https://xkcd.com/color/rgb/
    shapes = [".",",","v","^","<",">","1","2","s","P","*","H","+","X","D","d","|","_"]
    #To store the data to display
    time_data = [] 
    temp_data = []
    for i in range(number_sensors):  
        time_data.append([])
        temp_data.append([])
    #Open the file that we are going to write the data to
    t_file = open('temperatures_plot.txt', 'w')
    #Write the header
    t_file.write('#Date: ')
    t_file.write(time.asctime( time.localtime(time.time())))
    t_file.write('\n#\n')
    t_file.write('#execution time,   temperature,   sensor name\n#\n')
    start_time = time.time()  #We start to count the time here    

    def data_gen():
        while True:
            time_elapsed = time.time() - initial_time
            if time_elapsed >= interval : 
                initial_time = time.time()
                current_time = []
                temp = []
                for sensor in list_sensors:
                    current_time.append( time.time() - start_time )
                    temp.append( sensor.get_temperature() )
                #Write to the file every time we call the function
                for i in range(number_sensors):
                    t_file.write(str(current_time[i]) + ',' + str(temp[i]) + ',' + str(list_names[i]) + '\n')
                yield current_time, temp  #returns generator object


    def init():
        plt.title('TEMPERATURE SENSORS')
        plt.xlabel('time (s)')
        plt.ylabel('temperature (C)')
        ax.set_ylim(10, 50)
        ax.set_xlim(0, 10)

        i = 0
        for line in lines: 
            line.set_data(time_data[i], temp_data[i])
            line.set_linestyle('None')
            line.set_marker('o')
            line.set_markersize(3)
            line.set_markerfacecolor(colors[i])
            line.set_markeredgecolor(colors[i])
            line.set_label(list_names[i])
            if i >= 7 and  i <= 15: #If there are no more colors to chose from, change shapes
                warnings.warn('Not enough colors', UserWarning)
                i = 0
                line.set_marker(shapes[i])
            elif i > 15: #It can represent up to 15 different sensors. It woudn't make sense to have more in 1 graphic
                raise Exception ('Not enough colors and shapes to draw your data. Please, select less sensors to represent.')    
            ax.legend()          
            i = i + 1

        return lines
   
    def run(data):

        # update the data
        current_time, temp = data
        i = 0
        print('temp: ', temp)
        for line in lines :          
            time_data[i].append(current_time[i])
            temp_data[i].append(temp[i])
            xmin, xmax = ax.get_xlim()
            ymin, ymax = ax.get_ylim()
            #If the newdata doesn't fit the limits of the canvas, redraw it
            if temp [i] >= ymax:
                ax.set_ylim(ymin - (temp[i] - ymin) / 2, ymax + (ymax - temp[i]) / 2)
                ax.figure.canvas.draw() # To force a re-draw
            if current_time[i] >= xmax:
                ax.set_xlim(xmin, 2*xmax)
                ax.figure.canvas.draw() 
            line.set_data(time_data[i], temp_data[i])
            i = i + 1
        plt.savefig('temp.png') #Save the figure at every iteration

        return lines

    ani = animation.FuncAnimation(fig, run, data_gen, blit=False, interval=10,
        repeat=False, init_func=init)
    plt.show(block = False)

def setup():
    '''The setup function reads from the configuration file (if any was given as a 
    command line argument) and extracts all the possible parameters from there.
    The interval can be given through the config file or through a command line argument
    (cla), if it is given both ways the cla has priority over the file.
    If configuration file was NOT given:
    - If the interval was not given through cla it raises a NotEnoughInputsError
    - Sets defaults values to dns, server_name, service_address and data_file variables
    '''
    interval_cla, config_file, data_file_cla, plotting, daemon, pid_file, \
    user_name = command_line_arguments()
    if config_file:   #If a file was submitted we collect all the data
        number_sensors, list_sensors, list_addresses, list_names, dns, \
        server_name, service_address, data_file, interval, write_file, \
        write_telegraf, write_PyDIM = config(config_file)
    else:   #If not:
        # We collect the data found by the RBPi
        number_sensors, list_sensors, list_addresses = config(config_file)            
        #Set default variables:
        dns = 'infmag01'
        server_name = 'LabTemperatureMonitoring'
        service_address = 'localhost:8186'
        data_file = 'temperatures.txt'
        print('SET DEFAULT: dns, server_name, service_address, data_file')
        interval = None #The interval has no default value
        #We set to true the lists which enable to write to the file, telegraf and PyDIM
        [write_file, write_telegraf, write_PyDIM] = [[True]*number_sensors]*3
        #The list of names will be an enumeration of the sensors
        list_names = []
        for i in range(number_sensors):
            list_names.append(i)
        #If a config file was not submitted, the interval  should have been given through a cla
        if not interval_cla:
            raise NotEnoughInputsError('An interval in seconds has to be issued either ' \
            'from a config file or from the command line (e.g.: --interval 30)', 0)
            sys.exit(1)
    #If an interval or a file name have been given through cla, they have priority
    if interval_cla:
        interval = interval_cla
    if data_file_cla:
        data_file = data_file_cla
    if not daemon:  
        print('dns: ', dns)
        print('server_name: ', server_name)
        print('service_address: ', service_address)
        print('data_file: ', data_file)
        print('interval: ', interval)
        print ('list_names: ', list_names)
        print('number_of_sensors: ', number_sensors)
        print('list_addresses: ', list_addresses)

    return (plotting, number_sensors, list_sensors, list_addresses, list_names, interval, \
           dns, server_name, service_address, data_file, write_file, write_telegraf, \
           write_PyDIM, daemon, pid_file, user_name)

def exit_gracefully(signal, frame):
    '''Closes the file, sends an exit message and exits the program.
    '''
    t_file.close()
    print('Graceful exit!')
    sys.exit(0)

def main():
    '''Calls the setup function.  If daemon == True through cla, runs as a daemon. 
    If the plot  == True and daemon == False is given through a cla, plots live and writes to file.
    If not, it runs a loop,  writing the temperatures to a file, PyDIM and telegraf 
    at the set interval.
    '''
    (plotting, number_sensors, list_sensors, list_addresses, list_names, interval, \
    dns, server_name, service_address, data_file, write_file, write_telegraf, \
    write_PyDIM, daemon, pid_file, user_name) = setup()
    if daemon == True:
        create_daemon(pid_file, user_name)
    if daemon == False and plotting == True:
        plot_live_and_file(number_sensors, list_sensors, list_names, interval)
    else:
        global t_file #Declare the variable global so that we can close it if there is a keyboard interrupt
        t_file = write_header(number_sensors, data_file, list_names, write_file)        
        list_services = init_pydim(number_sensors, list_names, write_PyDIM, dns, server_name)
        time_elapsed = interval  #Don't have to wait to write the first data
        print('WRITING FILE...')
        while True:
            initial_time = time.time() #Return the time in seconds since the epoch as a floating point number
            #Does what the program has to do
            #---------------------------------------------
            list_temp = read_temperatures(list_sensors, list_names)
            write_to_file(number_sensors, t_file, list_temp, write_file)
            write_to_telegraf(number_sensors, list_temp, list_names, write_telegraf, service_address)
            write_to_pydim(number_sensors, list_services, list_temp, write_PyDIM)
            #---------------------------------------------
            time_elapsed = time.time() - initial_time
            sleeping_time = interval - time_elapsed
            if sleeping_time < 0:
                warnings.warn('The interval is shorter than the running time of the program.', UserWarning)
                continue  #Don't sleep (goes to next iteration)
            time.sleep(sleeping_time)


    
      
if __name__ == "__main__":
    signal.signal(signal.SIGINT, exit_gracefully)
    main()

 
  
