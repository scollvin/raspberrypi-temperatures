from w1thermsensor import W1ThermSensor
import argparse
def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

"""
    Mode	Resol	Conversion time
    9 bits	0.5°C	    93.75 ms
    10 bits	0.25°C	    187.5 ms
    11 bits	0.125°C	    375 ms
    12 bits	0.0625°C	750 ms
    Set the precision of the sensor for the next readings.
    If the ``persist`` argument is set to ``False`` this value
    is "only" stored in the volatile SRAM, so it is reset when
    the sensor gets power-cycled.

    If the ``persist`` argument is set to ``True`` the current set
    precision is stored into the EEPROM. Since the EEPROM has a limited
    amount of writes (>50k), this command should be used wisely.

    Note: root permissions are required to change the sensors precision.
    Note: This function is supported since kernel 4.7.

    :param int precision: the sensor precision in bits.
        Valid values are between 9 and 12 
        DEFAULT: 9
    :param bool persist: if the sensor precision should be written
        to the EEPROM.
        DEFAULT: False
"""
parser = argparse.ArgumentParser()
parser.add_argument("--permanence", "-p", type = str, default = False, 
    help = "False = resolution stored in the volatile SRAM, True = resolution stored in EEPROM, default to False")
parser.add_argument("--resolution", "-r", type = int, default = 9, choices = [9, 10, 11, 12], 
    help = "changes the resolution of the temperature sensors, default to 9 bits" )

args = parser.parse_args()
if args.permanence:
    permanence = str2bool(args.permanence)
print('permanence throug CLA: ', permanence)
if args.resolution:
    resolution = args.resolution
print('resolution through CLA: ', resolution)
#persist = True writes it to the EEPROM memory (it is still there if you reboot it)
list_sensors = W1ThermSensor.get_available_sensors() 
for sensor in list_sensors:
    sensor.set_precision(resolution, persist = permanence) #Have to run the script as sudo for that comand