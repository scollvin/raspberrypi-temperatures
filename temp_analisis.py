#!/usr/bin/python3
import csv
import os
import argparse
import matplotlib.pyplot as plt
import matplotlib.dates as mdate
import time as tm

def str2bool(v):
    '''Converts any string that can be understood as a boolean to a boolean'''
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
 
def read_temp_file(file_path):
    '''Reads the sensor data from the output file a returns a tuple with:
        - list_time, list with time stamp (epoch)
        - raw_sensor_data, list of lists that store temperature data for every sensor
        - list_names, list with sensor names
        '''
    list_time = []
    list_names = []
    #Read and store the data in arrays
    with open(file_path,'r') as csvfile:
        rows = csv.reader(csvfile, delimiter=',')
        count = 0
        for row in rows:
            if count == 2:
                for j in range(1, len(row)-1): #The first element is the timestamp and the last one an empty string
                    sensor_name = row[j].lstrip()
                    list_names.append(sensor_name)
                print(list_names)
                #Create empty 2-d array to store the sensor data
                raw_sensor_data = []
                for i in range(len(list_names)):
                    raw_sensor_data.append([])

            if row[0][0] != '#': #read the lines that are not commented
                list_time.append(float(row[0]))
                for i in range(len(list_names)): #Go through the temperatures
                    temp = float(row[i+1])
                    if temp != 85 and temp != 127.5: #Those are temperature errors
                        raw_sensor_data[i].append(temp)
                    else:
                        raw_sensor_data[i].append(None) #append the same value as the previous measure

            count = count + 1
    return list_time, raw_sensor_data, list_names

def command_line_arguments():
    '''Defines the arguments that can be sent through the command line and returns their value.
    That is:
        - file_path, the absolute path to the temperature file we want to analize
        - plot, boolean value to set or unset the plotting of the data
    '''
    parser = argparse.ArgumentParser()

    parser.add_argument("--file_path", "-f", type = str, default = '/tempmon_data/temperatures.txt',\
        help="path to the temperature file we want to analize")
    parser.add_argument("--plot", "-p", type = str, default = 'False',\
        help="boolean value to set or unset the plotting of the data")

    args = parser.parse_args()
    file_path = args.file_path
    plot = str2bool(args.plot)

    return file_path, plot

def strip_data(raw_sensor_data):
    '''Removes all the none integers from the list of temperatures
    and returns the same list without the impure data.
    '''
    sensor_data = []
    for impure_list_temp in raw_sensor_data:
        list_temp = []
        for temp in impure_list_temp:
            if temp != None:
                list_temp.append(temp)
        #list_temp =  [temp for temp in impure_list_temp if temp != None]
        #Substitute the sensor_data elements with the stripped lists
        sensor_data.append(list_temp)

    return sensor_data

def average_temp(sensor_data):
    '''Calculates the average of each list in an array of temperature lists 
    and returns an array with all the averaged temperatures.
    '''
    list_averages = []
    for list_temp in sensor_data:
        aver = sum(list_temp)/len(list_temp)
        list_averages.append(aver)
    return list_averages

def plot_data(list_time, raw_sensor_data, list_names):
    '''Plot the data.
    '''
    formated_time = mdate.epoch2num(list_time)
    fig, ax = plt.subplots()

    for i in range(len(list_names)):
        plt.plot(formated_time, raw_sensor_data[i], label = list_names[i], lw = 0.5)#linestyle='--', marker='o')

    # Choose your xtick format string
    date_fmt = '%d-%m-%y %H:%M:%S'
    # Use a DateFormatter to set the data to the correct format.
    date_formatter = mdate.DateFormatter(date_fmt)
    ax.xaxis.set_major_formatter(date_formatter)
    # Sets the tick labels diagonal so they fit easier.
    fig.autofmt_xdate()

    plt.xlabel('time (s)')
    plt.ylabel('temperature (Cº)')
    plt.title('Lab temperature monitoring')
    plt.legend()
    plt.savefig('temperature_plot.png')
    plt.show()

def detect_peaks(raw_sensor_data, list_time, list_averages):
    '''Returns a list with lists of times where the temperature is 4ºC 
    higher than the average for each sensor.
    '''
    number_sensors = len(raw_sensor_data)
    one_peak_time = []
    one_peak_temp = []
    peaks_time = []
    peaks_temp = []
    in_peak = False # We start outside a peak
    for i in range(number_sensors):
        peaks_time.append([])
        peaks_temp.append([])
    
    for i in range(number_sensors):
        list_temp = raw_sensor_data[i]
        aver = list_averages[i]      
        for j in range(len(list_temp)):
            time = list_time[j]
            temp = list_temp[j]
            if temp != None:
                prev_in_peak = in_peak
                if abs(list_temp[j] - aver) > 4 :
                    in_peak = True
                    one_peak_temp.append(temp)
                    one_peak_time.append(time)  
                else:
                    in_peak = False
                if prev_in_peak == True and in_peak == False: #We've exited the peak
                    max_peak_temp = max(one_peak_temp)
                    index = one_peak_temp.index(max_peak_temp)
                    max_peak_time = one_peak_time[index]
                    peaks_time[i].append(max_peak_time)
                    peaks_temp[i].append(max_peak_temp)
                    #Clear the lists in which we store the data for one peak
                    one_peak_time = []
                    one_peak_temp = []
    return peaks_time, peaks_temp

def write_peaks(peaks_time, peaks_temp, list_names, peaks_file):
    '''Write the peaks time and temperature to peaks_file.
    '''
    with open(peaks_file, 'w') as pfile:
        number_sensors = len(peaks_time)
        for i in range(number_sensors):
            number_peaks = len(peaks_time[i])
            pfile.write(list_names[i] + '\n')

            for j in range(number_peaks):
                time_tuple = tm.localtime(peaks_time[i][j])
                time_str = 'day: ' + str(time_tuple[2]) + ' ' + \
                str(time_tuple[3]) + ':' + str(time_tuple[4]) + ':' + str(time_tuple[5])
                pfile.write(time_str + ',' + str(peaks_temp[i][j]) + '\n')



def main():
    file_path, plot = command_line_arguments()
    list_time, raw_sensor_data, list_names = read_temp_file(file_path)
    #Remove the None values before calculating the average
    sensor_data = strip_data(raw_sensor_data)
    list_averages = average_temp(sensor_data)
    for i in range(len(sensor_data)):
        print('average temperature ' + list_names[i], list_averages[i])
    if plot:
        plot_data(list_time, raw_sensor_data, list_names)
    peaks_time, peaks_temp = detect_peaks(raw_sensor_data, list_time, list_averages)
    peaks_file = 'peaks.txt'
    write_peaks(peaks_time, peaks_temp, list_names, peaks_file)



if __name__ == "__main__":
    main()